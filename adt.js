const fs = require('fs');
const request = require('request');
const myArgs = process.argv.slice(2);
const json = require('./adt.json');

const getArg = (name) => {
    const index = myArgs.findIndex((el) => el === name);
    if(index > -1) {
        return myArgs[index + 1];
    }
    return null;
};
const hasArg = (name) => {
    const index = myArgs.findIndex((el) => el === name);
    return index > -1;
};

let id = getArg('-id');
let filePath = getArg('-file');
const name = getArg('-name');
const create = hasArg('-create');
const isImport = hasArg('-import');

const updateTemplate = () => {

    if(name) {
        filePath = json[name].file;
        id = json[name].id;
    }

    fs.readFile(filePath, 'utf8', (err, script) => {
        const formData = {
            cmd: '{"/dby.dbysite/update-ddm-template": {}}',
            templateId: id,
            script: script
        };
        makeRequest(formData);
    });
}
const importTemplate = () => {
    // TODO
};

const createTemplate = () => {
    fs.readFile(filePath, 'utf8', (err, script) => {
        const formData = {
            cmd: '{"/dby.dbysite/create-ddm-template": {}}',
            name: name,
            script: script
        };
        makeRequest(formData);
    });
};

const makeRequest = (formData) => {    
    const auth = Buffer.from('rui.cunha@pamodi.com:test').toString('base64');
    const options = {
        method: 'post',
        url: 'http://localhost:8080/api/jsonws/invoke',
        headers: {
            'Authorization': `Basic ${auth}`,
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        formData: formData
    };
    request(options, (error, response, body) => {
        if(response.statusCode === 200) {
            console.log('Success at: ' + new Date());
        } else {
            console.log('ERROR: ' + response.statusCode);
            if(typeof body === 'string') {
                body = JSON.parse(body);
            }
            if(body.exception) {
                console.log(body.exception);
            }
        }
    });
};


if (create) {
    createTemplate();
} else if (isImport) {
    importTemplate();
} else {
    updateTemplate();
}
