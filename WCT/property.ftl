<#assign articleId = .vars['reserved-article-id'].data />
<#assign description = .vars['reserved-article-description'].data />
<#assign displayPrice = true />

<div class="property-details">
    <a href="/properties" class="btn btn-sm btn-info mb-3 px-4">
        <i class="fa fa-long-arrow-left mr-2" aria-hidden="true"></i>Back
    </a>
    <div class="property-top d-flex justify-content-between align-items-end mb-4">
        <h2 class="mb-0">
            ${.vars['reserved-article-title'].data}
        </h2>
        <#if sold?has_content> 
            <#if getterUtil.getBoolean(sold.getData())>
                <#assign displayPrice = false >
                <span class="property-sold">
                    Sold!
                </span>
            </#if>
        </#if>

        <#if displayPrice > 
            <#if price.getData()?has_content> 
            <h3 class="property-price mb-0 text-right">
                <div>${price.getData()}€</div>
                <#if price.negociable?has_content> 
                <#if getterUtil.getBoolean(price.negociable.getData())>
                    <span class="property-price-negociable text-muted">
                        Negociable
                    </span>
                </#if>
                </#if>
            </h3>
            </#if>
        </#if>
    </div>
    <div class="container-fluid p-0">
        <div class="row">
            <div class="col-md-8 slide-show-h-400">
            	<#assign preferencesMap = {"collectionId":  "4", "hideTemplateSelector": "true", "displayStyle", "ddmTemplate_IMAGE-GALLERY-CAROUSEL-COMPLETE-FTL"} />
                <@liferay_portlet["runtime"]
                    defaultPreferences=freeMarkerPortletPreferences.getPreferences(preferencesMap)
                    instanceId="INSTANCE_ImgGallery_property_${articleId}"
                    portletName="com_doneby_image_gallery_portlet_ImageGalleryPortlet"
                />
            </div> 
            <div class="col-md-4">
                <div>
                    <h6 class="property-details-section-title">
                        <i class="fa fa-info mr-2" aria-hidden="true"></i>Basic info
                    </h6>
                    <div class="property-details-section px-2 pb-2">
                        <div class="d-flex flex-wrap">
                            
                            <#if BasicInfo.ref.getData()?has_content> 
                            <div class="flex-fill w-50 mt-2">
                                Rerence: <b>${BasicInfo.ref.getData()}</b>
                            </div>
                            <#else>
                                <div class="flex-fill w-50 mt-2">
                                    Rerence: <b>${articleId}</b>
                                </div>
                            </#if> 

                            <#if BasicInfo.type.getData()?has_content> 
                            <div class="flex-fill w-50 mt-2">
                                Type: <b>${BasicInfo.type.getData()}</b>
                            </div>
                            </#if> 

                            <#if BasicInfo.year.getData()?has_content> 
                            <div class="flex-fill w-50 mt-2">
                                Year: <b>${BasicInfo.year.getData()}</b>
                            </div>
                            </#if> 

                            <#if BasicInfo.bedrooms.getData()?has_content> 
                            <div class="flex-fill w-50 mt-2">
                                Bedrooms: <b>${BasicInfo.bedrooms.getData()}</b>
                            </div>
                            </#if> 

                            <#if BasicInfo.bathrooms.getData()?has_content> 
                            <div class="flex-fill w-50 mt-2">
                                Bathrooms: <b>${BasicInfo.bathrooms.getData()}</b>
                            </div>
                            </#if> 

                            <#if BasicInfo.area.getData()?has_content> 
                            <div class="flex-fill w-50 mt-2">
                                Area: <b>${BasicInfo.area.getData()}</b>
                            </div>
                            </#if> 
                            <#if BasicInfo.infoLabel.getSiblings()?has_content> 
                                <#list BasicInfo.infoLabel.getSiblings() as infoLabel> 
                                    <#if infoLabel.getData()?has_content> 
                                        <#if infoLabel.infoValue.getData()?has_content> 
                                        <div class="flex-fill w-50 mt-2">
                                            ${infoLabel.getData()}: <b>${infoLabel.infoValue.getData()}</b>
                                        </div>
                                        </#if> 
                                    </#if> 
                                </#list> 
                            </#if>
                            
                        </div>
                    
                    </div>
                </div>
                <#if Features.feature?has_content>
                <div class="mt-4">
                    <h6 class="property-details-section-title">
                        <i class="fa fa-list-ul mr-2" aria-hidden="true"></i>Features
                    </h6>
                    <div class="property-details-section px-2 pb-2">
                        <div class="d-flex flex-wrap">
                        <#if Features.feature.getSiblings()?has_content> 
                            <#list Features.feature.getSiblings() as feature> 
                                <#if feature.getData()?has_content> 
                                <div class="flex-fill w-50 mt-2">
                                    <i class="fa fa-check mr-2" aria-hidden="true"></i>${feature.getData()} 
                                </div>
                                </#if>
                            </#list> 
                        </#if>
                        </div>
                    </div>
                </div>
                </#if>

                <div class="mt-4">
                    <h6 class="property-details-section-title">
                        <i class="fa fa-comments-o mr-2" aria-hidden="true"></i>Contacts
                    </h6>
                    <div class="property-details-section px-2 pb-2">
                       
                        <div class="flex-fill w-50 mt-2">
                            <i class="fa fa-mobile mr-2" aria-hidden="true"></i>
                            <a href="tel:003519025558847">902 555 8847</a>
                        </div>
                        <div class="flex-fill w-50 mt-2">
                            <i class="fa fa-envelope-o mr-2" aria-hidden="true"></i>
                            <a href="mailto:geral@hubroot.pt">geral@hubroot.pt</a>
                        </div>
        
                    </div>
                </div>

                <div class="mt-4">
                    <div class="sharethis-inline-share-buttons"></div>
                </div>
                
            </div>
        </div>

        <#if description?has_content> 
            <div class="row">
                <div class="col-md-8">
                    <div class="mt-5">
                        <div class="paragraph-tick"></div>
                        ${description}
                    </div>
                </div>
            </div>
        </#if>
        <div class="row mt-5">
            <div class="col-md-8">
                <#if Location?has_content>
                <div class="mb-2">
                    <h6 class="property-details-section-title">
                        <i class="fa fa-map-marker mr-2" aria-hidden="true"></i>Location
                    </h6>
                    <div class="property-details-section px-2 pb-2">
                        <div class="d-flex flex-wrap">
                            <#if Location.addressLine1.getData()?has_content> 
                                <div class="flex-fill w-50 mt-2">
                                    Address: <b>${Location.addressLine1.getData()}</b>
                                </div>
                            </#if>
                            <#if Location.addressLine2.getData()?has_content> 
                                <div class="flex-fill w-50 mt-2">
                                    Address 2: <b>${Location.addressLine2.getData()}</b>
                                </div>
                            </#if>
                            <#if Location.postcode.getData()?has_content> 
                                <div class="flex-fill w-50 mt-2">
                                    Postcode: <b>${Location.postcode.getData()}</b>
                                </div>
                            </#if>
                            <#if Location.city.getData()?has_content> 
                                <div class="flex-fill w-50 mt-2">
                                    City: <b>${Location.city.getData()}</b>
                                </div>
                            </#if>
                            <#if Location.region.getData()?has_content> 
                                <div class="flex-fill w-50 mt-2">
                                    Region: <b>${Location.region.getData()}</b>
                                </div>
                            </#if>
                            <#if Location.postcode.getData()?has_content> 
                                <div class="flex-fill w-50 mt-2">
                                    Country: <b>${Location.country.getData()}</b>
                                </div>
                            </#if>
                        </div>
                    </div>
                </div>
                </#if>
                
                <#assign latitude = 0> 
                <#assign longitude = 0> 
                <#if geolocation.getData()?has_content>
                    <#if (geolocation.getData() != "")>
                        <div class="mt-4">
                            <#assign geolocationJSONObject = jsonFactoryUtil.createJSONObject(geolocation.getData())> 
                            <#assign latitude = geolocationJSONObject.getDouble("latitude") > 
                            <#assign longitude = geolocationJSONObject.getDouble("longitude") > 
                            <@liferay_map["map-display"] geolocation=true latitude=latitude longitude=longitude name="geolocation${randomizer.nextInt()}" />
                        </div>
                    </#if>
                </#if>
            </div>
        </div>
    <div>
</div>





