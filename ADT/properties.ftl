<#if entries?has_content> 
    <#assign size = entries?size />
    <#assign colClass = "col-lg-3" />
    <#assign rowClass = "justify-content-center" />
    <#if (size == 1) >
        <#assign colClass = "col-lg-5" />
    <#elseif (size == 2)>
        <#assign colClass = "col-lg-5" />
    <#elseif (size == 3)>
        <#assign colClass = "col-lg-4" />
    </#if>
    <#assign colClass = "col-md-4" />

    <#assign dbyAssetLayoutLocalService = serviceLocator.findService("com.doneby.services.service.DbyAssetLayoutLocalService")/>
    <#assign dbyImageLocalService = serviceLocator.findService("com.doneby.services.service.DbyImageLocalService")/>
    
    <div class="container">
        <div class="row ${rowClass} properties">
            <#list entries as entry>
                <#assign assetRenderer = entry.getAssetRenderer() />
                <#assign article = assetRenderer.getArticle()/>
                <#assign entryTitle = htmlUtil.escape(assetRenderer.getTitle(locale)) />
                <#assign entryDescription = article.getDescription(locale) />

                <#assign article = assetRenderer.getArticle()/>
                <#assign doc = saxReaderUtil.read(article.getContentByLocale(locale)) />
              
                <#assign images = [] />
                <#if doc.selectSingleNode("/root/dynamic-element[@name='mainImageId']")?has_content> 
                    <#assign imageNode = doc.selectSingleNode("/root/dynamic-element[@name='mainImageId']/dynamic-content") />
                    <#assign mainImage = dbyImageLocalService.fetchDbyImage(imageNode.getText()?number)/>
                    <#assign images = [mainImage] />
                </#if>

                
                <#if doc.selectSingleNode("/root/dynamic-element[@name='imagesCollectionId']")?has_content> 
                    <#assign collectionIdNode = doc.selectSingleNode("/root/dynamic-element[@name='imagesCollectionId']/dynamic-content") />
                    <#assign images = dbyImageLocalService.findCollectionVisibleImages(collectionIdNode.getText()?number, images)/>
                </#if>


                <#assign soldNode = doc.selectSingleNode("/root/dynamic-element[@name='sold']/dynamic-content") />
                <#assign sold = soldNode.getText() />

                <#assign articleDescription = article.getDescription(locale) />
                
                <#assign assetViewURL = assetPublisherHelper.getAssetViewURL(renderRequest, renderResponse, entry) />
			    <#assign viewURL = assetRenderer.getURLViewInContext(renderRequest, renderResponse, assetViewURL) />
                <#assign assetId = entry.getEntryId()?string />
                <#assign assetUrl = dbyAssetLayoutLocalService.getAssetUrl(assetId, themeDisplay) />
                <#assign hasPage = false />

                <#if assetUrl?has_content>
                    <#assign hasPage = true />
                <#else>
                    <#assign assetUrl = assetPublisherHelper.getAssetViewURL(renderRequest, renderResponse, assetRenderer, entry, !stringUtil.equals(assetLinkBehavior, "showFullContent")) />
                </#if>

                <div class="${colClass} mb-3" >
                    <div class="property" >
                        <#if sold?has_content>
                            <#assign soldBool = soldNode.getText() />
                            <#if sold=="true"> 
                                <div class="property-sold" >
                                    <span>Sold</span>
                                </div>
                            </#if>
                        </#if>
                        <div class="lfr-meta-actions-wrapper" style="float: none">
                            <#if assetRenderer.hasEditPermission(themeDisplay.getPermissionChecker())>
                                <#if hasPage> 
                                <#else>
                                    <@getCreatePageIcon article.getArticleId() entry.getEntryId() entryTitle entryDescription />
                                </#if>
                                <@getEditIcon/>
                            </#if>
                        </div>

                        <div class="slide-show-h-250">
                            <div class="slide-show property-image">
                                <#if images?has_content>
                                    <#list images as image>
                                        <div class="slide">   
                                            <img data-lazy="${image.getAdaptedImage(600, '/o/dby/file/')}"/>
                                        </div>
                                    </#list>
                                </#if>
                            </div>
                        </div>
                        <div class="property-details px-3 pb-3">

                            <div class="d-flex my-2 property-info color-hbrt-grey justify-content-center">
                                <div class="mr-2">
                                    <i class="fa fa-bed" title="Bedrooms" aria-hidden="true"></i>
                                    <span><@getNodeContent document=doc nodeNames=["BasicInfo", "bedrooms"]/></span>
                                </div>
                                <div class="mr-2">
                                    <i class="fa fa-bath" title="Bathrooms" aria-hidden="true"></i>
                                    <span><@getNodeContent document=doc nodeNames=["BasicInfo", "bathrooms"] /></span>
                                </div>
                                <div class="mr-2">
                                    <i class="fa fa-superscript" title="Area" aria-hidden="true"></i>
                                    <span><@getNodeContent document=doc nodeNames=["BasicInfo", "area"] /></span>
                                </div>
                            </div>

                            <h5 class="property-title my-2 text-center">
                                <a href="${assetUrl}">${entryTitle}</a>
                            </h5>

                            <div class="text-center">   
                                <a href="${assetUrl}" class="btn px-4 btn-sm btn-hbrt-gold">View More</a>
                            </div>
                        </div>

                    </div>
                </div>    
            </#list> 
        </div> 
    </div> 
    <script>


        $(document).ready(function(){
            const element = $('.property-image');
            element.slick({
                dots: false,
                lazyLoad: 'ondemand',
                autoplay: false,
                autoplaySpeed: 8000,
                prevArrow: '<i class="fa fa-angle-left gallery-prev" aria-hidden="true"></i>',
                nextArrow: '<i class="fa fa-angle-right gallery-next" aria-hidden="true"></i>'
            });
        });

        <#if assetRenderer.hasEditPermission(themeDisplay.getPermissionChecker())>
            function setAssetPage(pageTitle, pageDescription, articleId, assetEntryId, el) {
                const data = {
                    cmd: '{"/dby.dbyassetlayout/create-page": {}}',
                    data: JSON.stringify({
                        title: pageTitle,
                        description: pageDescription,
                        groupId: themeDisplay.getScopeGroupId(),
                        parentFriendlyURL: "/properties",
                        layoutTemplateId: "porfolio-layout"
                    })
                };

                $.ajax({
                    url: '/api/jsonws/invoke?p_auth=' + Liferay.authToken,
                    data: data,
                    type: 'POST',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                    }
                }).done((response) => {
                    el.parentNode.removeChild(el);
                    addPortletToPage({
                        columnId: "column-3",
                        setAsAssetPage: true,
                        layoutId: response.plid,
                        assetId: assetEntryId,
                        portletName: "com_liferay_journal_content_web_portlet_JournalContentPortlet",
                        preferences: {
                            articleId: articleId,
                            assetEntryId: assetEntryId
                        }
                    }, () => {
                        
                    });
                });
            }
            function addPortletToPage(portletData, callback) {
                const data = {
                    cmd: '{"/dby.dbyassetlayout/add-portlet-to-page": {}}',
                    data: JSON.stringify(portletData)
                }
                var request = $.ajax({
                    url: '/api/jsonws/invoke?p_auth=' + Liferay.authToken,
                    data: data,
                    type: 'POST',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                    }
                }).done((response) => {
                    const rsp = JSON.parse(response);
                    if(rsp) {
                        if(callback) {
                            callback();
                        }
                        
                    }
                });
            }
        </#if>
        
    </script>
</#if>

<#macro getNodeContent document  nodeNames>
    <#assign nodePath = "/root">
    <#assign node = "">
    <#assign nodes = []>
    <#list nodeNames as nodeName>
        <#assign nodePath = nodePath + "/dynamic-element[@name='${nodeName}']">
    </#list> 
    <#assign nodePath = nodePath + "/dynamic-content">
    <#assign nodes = document.selectNodes(nodePath)>
    <#if nodes?has_content>
        <#assign node = nodes?first>
        ${node.getText()} 
    </#if>
</#macro>

<#macro getImageResolution imageJson>
	<#assign groupId = imageJson.getLong("groupId") />
	<#assign uuid = imageJson.getString("uuid") />
	<#assign imageName = imageJson.getString("name") />
	<#assign imageAlt = imageJson.getString("alt") />
	<#assign fileEntryId = imageJson.getLong("fileEntryId") />
	<#assign imageId = imageName?replace(" ", "+") />

	<#assign imageSizes = ['gallery-view-800', 'gallery-view-400', 'thumbnail-300x300']>
	<picture>
		<source media="(max-width:767px)" srcset="/o/adaptive-media/image/${fileEntryId}/gallery-view-800/${imageId}">
		<img src="/documents/${groupId}/${uuid}" alt="${imageAlt}">
	</picture>
</#macro>

<#macro getCreatePageIcon articleId assetEntryId title description>
    <#assign normalizedName = dbyAssetLayoutLocalService.getNormalizedName(title) >
    <div class="lfr-meta-actions asset-actions" onClick="setAssetPage('${title}', '${description}', '${articleId}', '${assetEntryId}', this)">
        <@liferay_ui["icon"]
            cssClass="icon-monospaced visible-interaction"
            icon="page"
            markupView="lexicon"
            message="Create Display Page"
            
        />
    </div>
</#macro>

<#macro getEditIcon>
    <#assign redirectURL = renderResponse.createRenderURL() />
    ${redirectURL.setParameter("mvcPath", "/add_asset_redirect.jsp")}
    ${redirectURL.setWindowState("pop_up")}
    <#assign editPortletURL = assetRenderer.getURLEdit(renderRequest, renderResponse, windowStateFactory.getWindowState("pop_up"), redirectURL)!"" />
    <#if validator.isNotNull(editPortletURL)>
        <#assign title = languageUtil.format(locale, "edit-x", entryTitle, false) />
        <div class="lfr-meta-actions asset-actions">
            <@liferay_ui["icon"]
                cssClass="icon-monospaced visible-interaction"
                icon="pencil"
                markupView="lexicon"
                message=title
                url="javascript:Liferay.Util.openWindow({id:'" + renderResponse.getNamespace() + "editAsset', title: '" + title + "', uri:'" + htmlUtil.escapeURL(editPortletURL.toString()) + "'});"
            />
        </div>
    </#if>
</#macro>