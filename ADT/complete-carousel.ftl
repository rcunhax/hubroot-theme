<#if entries?has_content> 
    <#assign size = entries?size >
    <#assign id = "glide_" + .now?long + "_" + size>

    <div class="glide ${id}">
        <div class="glide__track" data-glide-el="track">
            <ul class="glide__slides">
                <#assign imageList = [] >
                <#list entries as entry>
                    <#assign assetRenderer = entry.getAssetRenderer() >
                    <#assign article = assetRenderer.getArticle()>
                    <#assign entryTitle = htmlUtil.escape(assetRenderer.getTitle(locale)) >
                    <#assign article = assetRenderer.getArticle()>
                    <#assign doc = saxReaderUtil.read(article.getContentByLocale(locale)) >
                    <#assign imageNode = doc.selectSingleNode("/root/dynamic-element[@name='image']/dynamic-content") >
                    <#assign articleDescription = article.getDescription(locale) >
                    
                    <#assign image = jsonFactoryUtil.createJSONObject(imageNode.getText()) >
                    <#assign groupId = image.getLong("groupId") >
                    <#assign uuid = image.getString("uuid") >
                    <#assign imageDescription = image.getString("description") >

                    <#assign imageUrl = "/documents/${groupId}/${uuid}">
                    <#assign imageList = imageList + [imageUrl] >
                    <li class="glide__slide" >
                        <div class="slide" style="background-image:url('${imageUrl}');">
                            <div class="lfr-meta-actions asset-actions" style="float: none">
                                <@getEditIcon /> 
                            </div>
                            <#if imageDescription?has_content>
                                <p>${imageDescription}</p>
                            </#if>
                        </div>
                    </li>    
                </#list> 
            </ul>
         
        </div>
        <#if (size > 1) >

        <div class="glide__arrows" data-glide-el="controls">
            <button class="glide__arrow glide__arrow--left" data-ref="fadereveal[el]" data-glide-dir="<" data-sr-id="8" style="; visibility: visible;  -webkit-transform: scale(1); opacity: 1;transform: scale(1); opacity: 1;-webkit-transition: all 0.2s ease-in-out 0s, -webkit-transform 1s cubic-bezier(0.6, 0.2, 0.1, 1) 0s, opacity 1s cubic-bezier(0.6, 0.2, 0.1, 1) 0s; transition: all 0.2s ease-in-out 0s, transform 1s cubic-bezier(0.6, 0.2, 0.1, 1) 0s, opacity 1s cubic-bezier(0.6, 0.2, 0.1, 1) 0s; ">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                    <path d="M0 12l10.975 11 2.848-2.828-6.176-6.176H24v-3.992H7.646l6.176-6.176L10.975 1 0 12z"></path>
                </svg>
            </button>

            <button class="glide__arrow glide__arrow--right" data-ref="fadereveal[el]" data-glide-dir=">" data-sr-id="9" style="; visibility: visible;  -webkit-transform: scale(1); opacity: 1;transform: scale(1); opacity: 1;-webkit-transition: all 0.2s ease-in-out 0s, -webkit-transform 1s cubic-bezier(0.6, 0.2, 0.1, 1) 0s, opacity 1s cubic-bezier(0.6, 0.2, 0.1, 1) 0s; transition: all 0.2s ease-in-out 0s, transform 1s cubic-bezier(0.6, 0.2, 0.1, 1) 0s, opacity 1s cubic-bezier(0.6, 0.2, 0.1, 1) 0s; ">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                    <path d="M13.025 1l-2.847 2.828 6.176 6.176h-16.354v3.992h16.354l-6.176 6.176 2.847 2.828 10.975-11z"></path>
                </svg>
            </button>
        </div>

        <div class="d-flex justify-content-center">
            <div class="glide__image-bullets" data-glide-el="controls[nav]">
                <#list imageList as imageurl>
                    <div class="glide__image-bullet" data-glide-dir="=${imageurl?index}"  style="background-image:url('${imageurl}');">
                        <div class="glide__image-bullet_overlay"></div>
                    </div>
                </#list>
            </div>
        </div>
        </#if>
    </div>
    <script>
        (function(){
            const glideEl = document.querySelector('.${id}');
            const glide = new Glide(glideEl, { startAt: 0 }).mount();
            const selectedBullet;
            glide.on('move.after', (e) => {
                selectedBullet = glideEl.querySelector('.glide__bullet--active');
                if(selectedBullet) {
                    selectedBullet.scrollIntoView({behavior: "smooth"});
                }
            });
            glide.on('build.after', (e) => { });
        })();
    </script>
    
</#if>

<#macro getImageResolution imageJson>
	<#assign groupId = imageJson.getLong("groupId") />
	<#assign uuid = imageJson.getString("uuid") />
	<#assign imageName = imageJson.getString("name") />
	<#assign imageAlt = imageJson.getString("alt") />
	<#assign fileEntryId = imageJson.getLong("fileEntryId") />
	<#assign imageId = imageName?replace(" ", "+") />

	<#assign imageSizes = ['gallery-view-800', 'gallery-view-400', 'thumbnail-300x300']>
	<picture>
		<source media="(max-width:767px)" srcset="/o/adaptive-media/image/${fileEntryId}/gallery-view-800/${imageId}">
		<img src="/documents/${groupId}/${uuid}" alt="${imageAlt}">
	</picture>
</#macro>

<#macro getEditIcon>
	<#if assetRenderer.hasEditPermission(themeDisplay.getPermissionChecker())>
		<#assign redirectURL = renderResponse.createRenderURL() />
		${redirectURL.setParameter("mvcPath", "/add_asset_redirect.jsp")}
		${redirectURL.setWindowState("pop_up")}
		<#assign editPortletURL = assetRenderer.getURLEdit(renderRequest, renderResponse, windowStateFactory.getWindowState("pop_up"), redirectURL)!"" />
		<#if validator.isNotNull(editPortletURL)>
			<#assign title = languageUtil.format(locale, "edit-x", entryTitle, false) />
			<div class="lfr-meta-actions asset-actions">
				<@liferay_ui["icon"]
					cssClass="icon-monospaced visible-interaction"
					icon="pencil"
					markupView="lexicon"
					message=title
					url="javascript:Liferay.Util.openWindow({id:'" + renderResponse.getNamespace() + "editAsset', title: '" + title + "', uri:'" + htmlUtil.escapeURL(editPortletURL.toString()) + "'});"
				/>
			</div>
		</#if>
	</#if>
</#macro>