<#if entries?has_content> 
    <div class="site-map mb-4 mb-lg-0 text-center text-lg-left">
        <h4>${themeDisplay.getCompany().getName()}</h4>
        <nav class="nav flex-column">
        <#list entries as curPage> 
            <a class="nav-link p-0" href="${curPage.getFriendlyURL(locale)}">${curPage.getName(locale)} </a>
        </#list>
        </nav>
    </div> 
</#if>