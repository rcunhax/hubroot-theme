<#assign liferay_captcha = PortletJspTagLibs["/META-INF/liferay-captcha.tld"] />

<div class="p-2">
	<#if entries?has_content>
		<#list entries as curEntry>
			<@getField field=curEntry /> 
		</#list>
		
		<div class="form-group">
			<#if requireCaptcha>
				<@liferay_captcha["captcha"] url="${captchaURL}" />
			</#if>
		</div>
		
		<div class="text-center">
			<button type="submit" class="btn btn-primary w-100">Submit</button>
		</div>
	<#else>
		<p>Form has no fields</p>	
	</#if>
</div>
<#macro getField field>

	<#assign dataType = field.getDataType() > 
	
	<#assign name = field.getName() > 
	<#assign label = name > 
	<#if field.getLabel()?has_content>
		<#assign label = field.getLabel() > 
	</#if>
	
	<#assign placeholder = "" > 
	<#if field.getPlaceholder()?has_content>
		<#assign placeholder = field.getPlaceholder() > 
	</#if>
	
	<#assign required = "" > 
	<#assign requiredStar = "" > 
	<#if field.isRequired()>
		<#assign required = "required" >
		<#assign requiredStar = "*" >  
	</#if>
	
	<#assign multiple = "" > 
	<#if field.isMultiple()>
		<#assign multiple = "multiple" >
	</#if>
	
	<#assign name = namespace+name > 
	<div class="form-group">
	    <label for="${name}">${label} ${requiredStar}</label>
	    
	    <#if dataType == "text">
	    	<input type="text" ${required} class="form-control" id="${name}" name="${name}" placeholder="${placeholder}"/>
	    <#elseif dataType == "longText">
	    	<textarea ${required}  class="form-control" id="${name}" rows="3"  name="${name}" placeholder="${placeholder}"></textarea>	    	
	    <#elseif dataType == "number">
	    	<input type="number" ${required} class="form-control" id="${name}" name="${name}" placeholder="${placeholder}"/>
	    <#elseif dataType == "options">
	    	<#if field.getOptionsSelectionType()?has_content>
				<#assign optionsSelectionType = field.getOptionsSelectionType() > 
			<#else>
				<#assign optionsSelectionType = "select" > 
			</#if>
	    	
	    	<#if optionsSelectionType == "radio">
	    		<#list field.getOptions() as option>
					<div class="form-check">
					  <input class="form-check-input" type="radio" name="${name}" value="${option}">
					  <label class="form-check-label">
					    ${option}
					  </label>
					</div>
				</#list>
			<#elseif optionsSelectionType == "checkbox">
				<#list field.getOptions() as option>
					<div class="form-check">
				      <input class="form-check-input" type="checkbox" name="${option}_${option?index}" value="${option}">
				      <label class="form-check-label" >
				        ${option}
				      </label>
				    </div>
				</#list>
			<#else>
				<select ${multiple} class="form-control" id="${name}" name="${name}">
		    		<option>Select</option>
		    		<#if field.getOptions()?has_content>
			    		<#list field.getOptions() as option>
							<option value="${option}">${option}</option>
						</#list>
					</#if>
			    </select>
	    	</#if>
	    <#elseif dataType == "boolean">
	    	<input type="checkbox" ${required} class="form-control" id="${name}" name="${name}" placeholder="${placeholder}"/>
	    <#elseif dataType == "email">
	    	<input type="email" ${required} class="form-control" id="${name}" name="${name}" placeholder="${placeholder}"/>
	    <#elseif dataType == "tel">
	    	<input type="tel" ${required} class="form-control" id="${name}" name="${name}" placeholder="${placeholder}"/>
	    <#else>	
	    	<input type="text" ${required} class="form-control" id="${name}" name="${name}" placeholder="${placeholder}"/>
	    </#if>
	    
	</div>
	
</#macro>