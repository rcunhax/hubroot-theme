<#if entries?has_content>
    <div class="slide-show slide-hidden" id="${renderResponse.getNamespace()}_slide_show">

    	<#list entries as entry>
    		<#if entry?is_first> 
            	<div class="slide slide-place-holder" onclick="${renderResponse.getNamespace()}maximize(${entry?index})" 
            		style="background-image: url(${entry.getAdaptedImage(600, '/o/dby/file/')})"> 
		    	</div>
		    </#if>	
            <div class="slide" style="background-image: url('/o/dby/file/${entry.fileEntryId}');">    		    
                <#if entry?is_first> 
                   <div class="slide-text gallery-image-text ">
    					<p class="d-md-flex d-none justify-content-center slide-title">
    						<img src="/o/logo" />
    					</p>
    					<p class="slide-description">
    						COMÉRCIO E REABILITAÇÃO DE IMÓVEIS
    					</p>
    				</div>
                </#if>

    		</div>
    	</#list>   
	</div>

	<script type="text/javascript" charset="utf-8">
	    $(document).ready(function(){
	    	let counter = 0;
	    	let interval = setInterval(() => {
	    		counter++;
	    		if(counter == 30) {
	    			clearInterval(interval);
	    		}
	    		if($) {
	    			const element = $('#${renderResponse.getNamespace()}_slide_show');
	    			if(element.slick) {
    					const el = element[0];
    					el.removeChild(el.querySelector('.slide-place-holder'));
    					el.classList.remove('slide-hidden');
	    				element.slick({
	    		            dots: ${entries?size} > 1,
	    		            autoplay: false,
	    		            autoplaySpeed: 8000,
	    		            prevArrow: '<i class="fa fa-angle-left gallery-prev" aria-hidden="true"></i>',
	    		            nextArrow: '<i class="fa fa-angle-right gallery-next" aria-hidden="true"></i>'
	    		        });
    					clearInterval(interval);
	    			}    			
	    		}
	    	}, 100);
	    });
	</script>
</#if>