<!DOCTYPE html>

<#include init />

<html class="${root_css_class}" dir="<@liferay.language key="lang.dir" />" lang="${w3c_language_id}">

<head>
	<title>${the_title} - ${company_name}</title>

	<meta content="initial-scale=1.0, width=device-width" name="viewport" />
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,600|Lato:300,400,700|Montserrat:400,500|Quicksand:300,400,500|Thasadith:400,700|Open+Sans+Condensed:300,300i,700|Roboto:400,400i,500,500i,700" rel="stylesheet">
	<#if cookieConsent>
		<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.css" />
	</#if>
	<@liferay_util["include"] page=top_head_include />

	<link rel="stylesheet" href="${css_folder}/photoswipe.css"> 
	<link rel="stylesheet" href="${css_folder}/photoswipe-default-skin/default-skin.css"> 
	<script src="${javascript_folder}/photoswipe.min.js"></script> 

	<script src="${javascript_folder}/photoswipe-ui-default.min.js"></script> 
	<script> 
		function loadPhotoSwipe(images, index) {
			const doPhotoSwipe = () => {
				const pswpElement = document.querySelector('.pswp');
				const options = {
					index: index || 0
				};
				const gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, images, options);
				gallery.init();
			}
			doPhotoSwipe();
			
		}	
	</script>
	<@liferay_util["include"] page=inline_style />
</head>

<body class="${css_class}">


<@liferay_ui["quick-access"] contentId="#main-content" />

<@liferay_util["include"] page=body_top_include />

<@liferay.control_menu />

<div class="container-fluid" id="wrapper">
	<div class="container-fluid" >
		<div class="container container-sm-fluid">
			<div class="row">
				<div class="col-12">
					<header id="banner" role="banner" class="d-flex justify-content-md-between justify-content-center d-flex align-items-center my-2 my-md-2">
						<div id="mobile-nav-button"> 
							<div id="mobile-nav-icon"> 
								<span></span> 
								<span></span> 
								<span></span> 
								<span></span> 
							</div> 
						</div>
						<div id="heading">

							<#--  <@liferay_portlet["runtime"]
								portletName="com_doneby_logo_portlet_Logo"
							/>  -->

							<h1 class="site-title">
								<a class="${logo_css_class}" href="${site_default_url}" title="<@liferay.language_format arguments="${site_name}" key="go-to-x" />">
									<#if show_logo>
										<img alt="${site_name}" src="/o/logo" />
									</#if>
									<#if show_site_name>
										<span class="site-name" title="<@liferay.language_format arguments="${site_name}" key="go-to-x" />">
											${site_name}
										</span>
									</#if>
								</a>
							</h1>

						</div>
						<div class="navigation">
							<h2 class="site-name">${site_name}</h2>  
							<#-- <#if show_logo>
								<div class="site-title text-center">
									<img alt="${site_name}" src="/o/logo" />
								</div>
							</#if>-->
							<#if has_navigation && is_setup_complete>
								<#include "${full_templates_path}/navigation.ftl" />
							</#if>
							<div class="menu-social mt-4">
								<@social/> 
							</div>

							<div class="close-navigation"> 
								<i class="fa fa-long-arrow-left" aria-hidden="true"></i> 
							</div>
						</div>
					</header>
				</div>
			</div>
		</div>
	</div>
	<section id="content">
		<h1 class="hide-accessible">${the_title}</h1>

		<#if selectable>
			<@liferay_util["include"] page=content_include />
		<#else>
			${portletDisplay.recycle()}

			${portletDisplay.setTitle(the_title)}

			<@liferay_theme["wrap-portlet"] page="portlet.ftl">
				<@liferay_util["include"] page=content_include />
			</@>
		</#if>
	</section>

	<footer id="footer" role="contentinfo">
		<div class="company-info py-5">
			<div class="container">
				<div class="row">
					<div class="col-lg-6">
						<div class="d-flex flex-column">
							<div class="d-flex justify-content-center justify-content-md-start mb-3 mb-md-0 footer-logo">
								<#--  <img alt="${site_name}" src="${images_folder}/hubroot-logo-white.svg" />  -->
								<#include "${full_templates_path}/logo-svg.ftl" />
								<#--  <i class="icon-hubroot-leters"></i>  -->
							</div>
							<div class="text-center text-md-left mb-4 mb-lg-0">
								<@liferay_portlet["runtime"]
									instanceId="INSTANCE_DbyFreeTextPortlet_footer${group_id}"
									portletName="com_doneby_freetext_portlet_DbyFreeTextPortlet"
								/>
							</div>
						</div>

					</div>
					<div class="col-lg-3">
						<@liferay_portlet["runtime"]
							instanceId="INSTANCE_SiteNavigationSiteMapPortlet_footer${group_id}"
							portletName="com_liferay_site_navigation_site_map_web_portlet_SiteNavigationSiteMapPortlet"
						/>
					</div>
					<div class="col-lg-3 text-center text-lg-left">
						<h4>
						<#--  <@liferay.language key="footer.contacts" />  -->
							Contactos
						</h4>
						<div class="company-info-contact px-2 pb-2">
							<#if phone?has_content>
							<div class="flex-fill mt-2">
								<i class="fa fa-mobile mr-2" aria-hidden="true"></i>
								<a href="tel:${phone}">${phone}</a>
							</div>
							</#if>
							<#if email?has_content>
							<div class="flex-fill mt-2">
								<i class="fa fa-envelope-o mr-2" aria-hidden="true"></i>
								<a href="mailto:${email}">${email}</a>
							</div>
							</#if>
						</div>
						<@social/> 
					</div>
				</div>
			</div>
		</div>
		<div class="align-items-center content-info d-flex justify-content-center p-3 text-center">
			<#if copyright?has_content>
				<span class="mx-2">© ${the_year} ${copyright}</span>
			</#if>
			<#if doneby?has_content>
				<span class="mx-2">
					<span>Created by </span>
					<a href="http://${donebyUrl}" target="_blank">${doneby}</a>
				</span>
			</#if>
			<div class="ml-4">
				<@liferay_portlet["runtime"]
					portletName="com_liferay_site_navigation_language_web_portlet_SiteNavigationLanguagePortlet"
				/>
			</div>
		</div>

	</footer>
	<div id="overlay"></div>

	<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="pswp__bg"></div>
		<div class="pswp__scroll-wrap">
			<div class="pswp__container">
				<div class="pswp__item"></div>
				<div class="pswp__item"></div>
				<div class="pswp__item"></div>
			</div>
			<div class="pswp__ui pswp__ui--hidden">
				<div class="pswp__top-bar">
					<div class="pswp__counter"></div>
					<button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
					<button class="pswp__button pswp__button--share" title="Share"></button>
					<button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
					<button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
					<div class="pswp__preloader">
						<div class="pswp__preloader__icn">
						<div class="pswp__preloader__cut">
							<div class="pswp__preloader__donut"></div>
						</div>
						</div>
					</div>
				</div>
				<div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
					<div class="pswp__share-tooltip"></div> 
				</div>
				<button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
				</button>
				<button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
				</button>
				<div class="pswp__caption">
					<div class="pswp__caption__center"></div>
				</div>
			</div>
		</div>
	</div>
</div>


<@liferay_util["include"] page=body_bottom_include />

<@liferay_util["include"] page=bottom_include />
<script type="text/javascript" src="${javascript_folder}/slick.js"></script>
<#if cookieConsent>
	<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.js"></script>
	<script type="text/javascript">
		window.addEventListener("load", function() {
			if(window.cookieconsent) {
				window.cookieconsent.initialise({
					"palette": {
						"popup": {
							"background": "#000"
						},
						"button": {
							"background": "#f1d600"
						}
					},
					"content": {
						"message": "This website uses cookies to ensure you get the best experience on our website",
						"link": "Learn more"
					},
					"position": "bottom-right"
				});
			}
		});
	</script>
</#if>
<script type="text/javascript">
	function closeNavigation() {
		$('body').toggleClass("menu-on");
	}
	$('#mobile-nav-button').click(function(){
		closeNavigation();
	});
	$('#overlay').click(function(){
		closeNavigation();
	});
	$('.close-navigation').click(function(){
		closeNavigation();
	});		
</script>
	
<#if is_signed_in>
	<@liferay_portlet["runtime"]
		portletName="com_doneby_settings_portlet_DbySettings"
	/>
	<script src="${javascript_folder}/ckeditor.js"></script> 
</#if>

<#if underConstruction>
	<div class="d-flex flex-column justify-content-center align-items-center" style="color: #FFF; background-color: rgba(0,0,0,0.6); position: fixed; width: 100%; height: 100%; z-index: 999999999; top: 0; left: 0" id="underConstruction">
		<button class="btn" type="button" style="width: 50px; height: 50px; position: absolute; right: 20px; top: 20px;  z-index: 2;" onclick="document.getElementById('underConstruction').remove()">
			<img src="${images_folder}/close.svg" style="height: 100%"  role="button" tabindex="0"/>
		</button>
		
		<img src="${images_folder}/digger.svg" style="height: 90px; margin-bottom: 30px"  role="button" tabindex="0"/>
		<h1 class="d-flex justify-content-center align-items-center">
			<span>Em construção</span>
		</h1>
		<h6>Brevemente</h6>
	</div>
</#if>

</body>

</html>