<#assign copyright = company_name>

<#assign donebyUrl = getterUtil.getString(theme_settings["donebyUrl"], "www.doneBy.co.uk") />
<#assign doneby = getterUtil.getString(theme_settings["doneBy"], "DoneBy Limited") />

<#assign instagram = getterUtil.getString(theme_settings["instagram"]) />
<#assign facebook = getterUtil.getString(theme_settings["facebook"]) />
<#assign twitter = getterUtil.getString(theme_settings["twitter"]) />
<#assign pinterest = getterUtil.getString(theme_settings["pinterest"]) />
<#assign youtube = getterUtil.getString(theme_settings["youtube"]) />
<#assign email = getterUtil.getString(theme_settings["email"]) />
<#assign phone = getterUtil.getString(theme_settings["phone"]) />
<#assign show_site_name = false />
<#assign show_logo = getterUtil.getBoolean(theme_settings["showLogo"], true) />
<#assign cookieConsent = getterUtil.getBoolean(theme_settings["cookieConsent"]) />
<#assign underConstruction = getterUtil.getBoolean(theme_settings["underconstruction"]) />

<#macro social>
	<div class="social nav-social">
		<ul>
			<#if facebook?has_content>
			<li>
				<a href="http://www.facebook.com/${facebook}" target="_blank">
					<i class="fa fa-facebook" aria-hidden="true"></i>
				</a>
			</li>
			</#if>
			<#if instagram?has_content>
			<li>
				<a href="https://www.instagram.com/${instagram}/" target="_blank">
					<i class="fa fa-instagram" aria-hidden="true"></i>
				</a>
			</li>
			</#if>
			<#if pinterest?has_content>
			<li>
				<a href="http://www.pinterest.com/${pinterest}" target="_blank">
					<i class="fa fa-pinterest" aria-hidden="true"></i>
				</a>
			</li>
			</#if>
			<#if youtube?has_content>
			<li>
				<a href="http://www.youtube.com/c/${youtube}" target="_blank">
					<i class="fa fa-youtube" aria-hidden="true"></i>
				</a>
			</li>
			</#if>
			<#if twitter?has_content>
			<li>
				<a href="http://www.twitter.com/${twitter}" target="_blank">
					<i class="fa fa-twitter" aria-hidden="true"></i>
				</a>
			</li>
			</#if>
			<#--  <#if email?has_content>
			<li>
				<a href="mailto:${email}">
					<i class="fa fa-envelope-o" aria-hidden="true"></i>
				</a>
			</li>
			</#if>  -->
		</ul>
	</div>
</#macro>