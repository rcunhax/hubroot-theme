AUI().ready(

	/*
	This function gets loaded when all the HTML, not including the portlets, is
	loaded.
	*/

	function() {
	}
);

Liferay.Portlet.ready(

	/*
	This function gets loaded after each and every portlet on the page.

	portletId: the current portlet's id
	node: the Alloy Node object of the current portlet
	*/

	function(portletId, node) {
	}
);

Liferay.on(
	'allPortletsReady',

	/*
	This function gets loaded when everything, including the portlets, is on
	the page.
	*/

	function() {
	}
);

var DBY_THEME = {
	setAssetPage: (pageTitle, pageDescription, articleId, assetEntryId, el) => {
		const data = {
			cmd: '{"/dby.dbyassetlayout/create-page": {}}',
			data: JSON.stringify({
				title: pageTitle,
				description: pageDescription,
				groupId: themeDisplay.getScopeGroupId(),
				parentFriendlyURL: "/properties",
				layoutTemplateId: "porfolio-layout"
			})
		};
		$.ajax({
			url: '/api/jsonws/invoke?p_auth=' + Liferay.authToken,
			data: data,
			type: 'POST',
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
			}
		}).done((response) => {
			el.parentNode.removeChild(el);
			addPortletToPage({
				columnId: "column-3",
				setAsAssetPage: true,
				layoutId: response.plid,
				assetId: assetEntryId,
				portletName: "com_liferay_journal_content_web_portlet_JournalContentPortlet",
				preferences: {
					articleId: articleId,
					assetEntryId: assetEntryId
				}
			});
		});
	},

	addPortletToPage: (portletData) => {
		const data = {
			cmd: '{"/dby.dbyassetlayout/add-portlet-to-page": {}}',
			data: JSON.stringify(portletData)
		}
		var request = $.ajax({
			url: '/api/jsonws/invoke?p_auth=' + Liferay.authToken,
			data: data,
			type: 'POST',
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
			}
		}).done((response) => {
			const rsp = JSON.parse(response);
			console.log(rsp);
		});
	},

	isElementVisible: (el) => {
		var rect     = el.getBoundingClientRect(),
			vWidth   = window.innerWidth || doc.documentElement.clientWidth,
			vHeight  = window.innerHeight || doc.documentElement.clientHeight,
			efp      = function (x, y) { return document.elementFromPoint(x, y) };     
	
		// Return false if it's not in the viewport
		if (rect.right < 0 || rect.bottom < 0 
				|| rect.left > vWidth || rect.top > vHeight)
			return false;
	
		// Return true if any of its four corners are visible
		return (
			  el.contains(efp(rect.left,  rect.top))
		  ||  el.contains(efp(rect.right, rect.top))
		  ||  el.contains(efp(rect.right, rect.bottom))
		  ||  el.contains(efp(rect.left,  rect.bottom))
		);
	}
}